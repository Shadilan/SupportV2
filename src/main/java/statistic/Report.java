package statistic;

import utility.DBUtils;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.*;

/**
 * Report Class
 */
public class Report {
    public static String doReport(HttpServletRequest request,HttpServletResponse response) {
        try {
            String operation;
            String key;
            try {
                operation = request.getParameter("Type");
                key = request.getParameter("Key");
            } catch (Exception e) {
                return "Operation not recognized.";
            }
            switch (operation) {
                case "Basic":
                    response.setContentType("text/html");
                    //response.setHeader("Content-Disposition","attachment; filename=" + (new Date()).toString()+".html" );
                    return basicReport(key);
                case "MultiDevice":
                    response.setContentType("text/html");
                    //response.setHeader("Content-Disposition","attachment; filename=" + (new Date()).toString()+".html" );
                    return multyDeviceReport(key);
                case "MultiAccount":
                    response.setContentType("text/html");
                    //response.setHeader("Content-Disposition","attachment; filename=" + (new Date()).toString()+".html" );
                    return multyAccountReport(key);
                case "Move":
                    String user = request.getParameter("User");
                    response.setContentType("application/vnd.google-earth.kml+xml kml");
                    response.setHeader("Content-Disposition", "attachment; filename=" + user + ".kml");
                    return movingReport(user);
                default:
                    return "Report not recognized.";
            }

        } catch (Exception e) {
            return e.toString() + Arrays.toString(e.getStackTrace());

        }
    }

    public static String basicReport(String key) {
        String result;
        Connection con = null;
        try {
            con = DBUtils.ConnectDB();
            PreparedStatement stmt;
            stmt = con.prepareStatement("select un.StatDay,un.User, un.Device, un.GroupName, un.Attribute, un.hit, un.AvgHit, un.MinHit, un.MaxHit, un.Time, un.AvgTime, un.MinTime, un.MaxTime \n" +
                    "from (\n" +
                    "SELECT s.StatDay,u.User, d.Device, attr.GroupName, attr.Attribute, s.hit, s.hit / num AvgHit, s.MinHit, s.MaxHit, s.Time, s.Time / num AvgTime, s.MinTime, s.MaxTime\n" +
                    "FROM Applications app JOIN Users u ON ( app.ID = u.ApplicationID )  JOIN Devices d ON ( u.ID = d.UserID )  JOIN Attributes attr ON ( app.ID = attr.ApplicationID )  JOIN Statistics s ON ( s.UserID = u.ID AND s.DeviceID = d.ID AND s.AttributeID = attr.ID )  WHERE u.User !=  '' AND app.AppKey =  ?\n" +
                    "union all\n" +
                    "select NULL StatDay,u.User, d.Device, attr.GroupName, attr.Attribute, s.hit, s.hit / num AvgHit, s.MinHit, s.MaxHit, s.Time, s.Time / num AvgTime, s.MinTime, s.MaxTime\n" +
                    "FROM Applications app JOIN Users u ON ( app.ID = u.ApplicationID )  JOIN Devices d ON ( u.ID = d.UserID )  JOIN Attributes attr ON ( app.ID = attr.ApplicationID )  JOIN Statistics_All s ON ( s.UserID = u.ID AND s.DeviceID = d.ID AND s.AttributeID = attr.ID )  WHERE u.User !=  '' AND app.AppKey =  ?) un\n" +
                    "ORDER BY un.StatDay,un.User, un.Device, un.GroupName, un.Attribute");
            stmt.setString(1, key);
            stmt.setString(2, key);
            ResultSet rs = stmt.executeQuery();
            rs.beforeFirst();
            result = "<table>";
            result += "<thead><tr><th>Date</th><th>User</th><th>Device</th><th>Group</th><th>Attribute</th><th>Hit</th><th>Avg</th><th>Min</th><th>Max</th><th>Time</th><th>Avg</th><th>Min</th><th>Max</th></tr></thead>";
            result += "<tbody>";
            while (rs.next()) {
                result += "<tr><th>" + rs.getDate(1) + "</th><th><a href='http://support-merchantarg.rhcloud.com/report.jsp?Type=Move&Key="+key+"&User=" +rs.getString(2)+"'>" + rs.getString(2) + "</a></th><th>" + rs.getString(3) + "</th><th>" + rs.getString(4) + "</th><th>" + rs.getString(5) + "</th><th>" + rs.getInt(6)
                        + "</th><th>" + rs.getFloat(7) + "</th><th>" + rs.getInt(8) + "</th><th>" + rs.getInt(9) + "</th><th>" + rs.getInt(10) + "</th><th>" + rs.getFloat(11)
                        + "</th><th>" + rs.getInt(12) + "</th><th>" + rs.getInt(13) + "</th></tr>";
            }
            result += "</tbody></table>";
        } catch (SQLException | NamingException e) {
            result = e.toString() + Arrays.toString(e.getStackTrace());
        }
        try {
            if (con!=null && !con.isClosed()) con.close();
        } catch (SQLException e) {
            //result+=e.toString()+Arrays.toString(e.getStackTrace());
        }
        return result;
    }
    public static String multyDeviceReport(String key) {
        String result;
        Connection con = null;
        try {
            con = DBUtils.ConnectDB();
            PreparedStatement stmt;
            stmt = con.prepareStatement("select  User, Device,min(StatDay),max(StatDay)\n" +
                    "from (\n" +
                    "    SELECT app.AppKey,u.User, d.Device,s.StatDay\n" +
                    "\tFROM Applications app \n" +
                    "\tJOIN Users u ON ( app.ID = u.ApplicationID )  \n" +
                    "\tJOIN Devices d ON ( u.ID = d.UserID )  \n" +
                    "\tJOIN Statistics s ON ( s.UserID = u.ID AND s.DeviceID = d.ID)  \n" +
                    "\tWHERE u.User !=  '' \t\n" +
                    "\tUNION all\n" +
                    "\tselect AppKey,User,Device,Creation\n" +
                    "\tfrom TRANSACTION\n" +
                    "\twhere User!=''\n" +
                    ") un\n" +
                    "where User in (select User from Devices di join Users ui on ui.id=di.UserID  group by User having count(1)>1)\n" +
                    "AND AppKey =  ?\n" +
                    "group by un.User,un.Device\n" +
                    "ORDER BY un.User, un.Device");
            stmt.setString(1, key);
            ResultSet rs = stmt.executeQuery();
            rs.beforeFirst();
            result = "<table>";
            result += "<thead><tr><th>User</th><th>Device</th><th>StartDate</th><th>EndDate</th></tr></thead>";
            result += "<tbody>";
            String oldrs="";
            String currs;
            while (rs.next()) {
                currs=rs.getString(1);
                if (currs.equals(oldrs)) currs="";
                result += "<tr><th><a href='http://support-merchantarg.rhcloud.com/report.jsp?Type=Move&Key="+key+"&User=" +rs.getString(1)+"'>" + currs + "</a></th><th>" + rs.getString(2) + "</th><th>" + rs.getDate(3) + "</th><th>" + rs.getDate(4) + "</th></tr>";
                oldrs=rs.getString(1);
            }
            result += "</tbody></table>";
        } catch (SQLException | NamingException e) {
            result = e.toString() + Arrays.toString(e.getStackTrace());
        }
        try {
            if (con!=null && !con.isClosed()) con.close();
        } catch (SQLException e) {
            //result+=e.toString()+Arrays.toString(e.getStackTrace());
        }
        return result;
    }
    public static String multyAccountReport(String key) {
        String result;
        Connection con = null;
        try {
            con = DBUtils.ConnectDB();
            PreparedStatement stmt;
            stmt = con.prepareStatement("select  Device,User,min(StatDay),max(StatDay)\n" +
                    "from (\n" +
                    "    SELECT app.AppKey,u.User, d.Device,s.StatDay\n" +
                    "\tFROM Applications app \n" +
                    "\tJOIN Users u ON ( app.ID = u.ApplicationID )  \n" +
                    "\tJOIN Devices d ON ( u.ID = d.UserID )  \n" +
                    "\tJOIN Statistics s ON ( s.UserID = u.ID AND s.DeviceID = d.ID)  \n" +
                    "\tWHERE u.User !=  '' \t\n" +
                    "\tUNION all\n" +
                    "\tselect AppKey,User,Device,Creation\n" +
                    "\tfrom TRANSACTION\n" +
                    "\twhere User!=''\n" +
                    ") un\n" +
                    "where Device in (select di.Device from Devices di join Users ui on ui.id=di.UserID where ui.User!='' group by Device having count(1)>1)\n" +
                    "AND AppKey =  ?\n" +
                    "group by un.User,un.Device\n" +
                    "ORDER BY un.Device,un.User\n");
            stmt.setString(1, key);
            ResultSet rs = stmt.executeQuery();
            rs.beforeFirst();
            result = "<table>";
            result += "<thead><tr><th>Device</th><th>User</th><th>StartDate</th><th>EndDate</th></tr></thead>";
            result += "<tbody>";
            String oldrs="";
            String currs;
            while (rs.next()) {
                currs=rs.getString(1);
                if (currs.equals(oldrs)) currs="";
                result += "<tr><th>" + currs + "</th><th><a href='http://support-merchantarg.rhcloud.com/report.jsp?Type=Move&Key="+key+"&User=" +rs.getString(2)+"'>" + rs.getString(2) + "</a></th><th>" + rs.getDate(3) + "</th><th>" + rs.getDate(4) + "</th></tr>";
                oldrs=rs.getString(1);
            }
            result += "</tbody></table>";
        } catch (SQLException | NamingException e) {
            result = e.toString() + Arrays.toString(e.getStackTrace());
        }
        try {
            if (con!=null && !con.isClosed()) con.close();
        } catch (SQLException e) {
            //result+=e.toString()+Arrays.toString(e.getStackTrace());
        }
        return result;
    }
    public static String movingReport(String user) {
        String result;
        Connection con = null;
        try {
            con = DBUtils.ConnectDB();
            PreparedStatement stmt_con;
            result = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<kml xmlns=\"http://www.opengis.net/kml/2.2\"> <Document>\n" +
                    " <name>" + user + "</name>\n" +
                    " <Style id=\"myStyle\">\n" +
                    " <LineStyle>\n" +
                    " <color>ff00ff00</color>\n" +
                    " <width>4</width>\n" +
                    " </LineStyle>\n" +
                    " <PolyStyle>\n" +
                    " <color>7f00ff00</color>\n" +
                    " </PolyStyle>\n" +
                    " </Style>";


            stmt_con = con.prepareStatement("select Lat,Lng,Speed,Time from Position where User=? order by Time");
            stmt_con.setString(1, user);
            String line = "";
            String points = "";
            ResultSet rs_con = stmt_con.executeQuery();
            if (rs_con.isBeforeFirst()) {
                line += "<Placemark>" +
                        " <name>Путь</name>\n" +
                        " <styleUrl>#myStyle</styleUrl>\n" +
                        " <LineString>\n" +
                        " <extrude>1</extrude>\n" +
                        " <tessellate>1</tessellate>\n" +
                        " <altitudeMode>absolute</altitudeMode>\n" +
                        " <coordinates>";
                String coord="";
                DateFormat df=DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.FULL);
                df.setTimeZone(TimeZone.getTimeZone("GMT+3"));
                while (rs_con.next()) {
                    if (!coord.equals((((float) rs_con.getInt("Lng")) / 1e6) + "," + (((float) rs_con.getInt("Lat")) / 1e6))) {
                        coord=(((float) rs_con.getInt("Lng")) / 1e6) + "," + (((float) rs_con.getInt("Lat")) / 1e6);
                        line +=coord +"\n";
                        Calendar cal=Calendar.getInstance();
                        Date dt=new Date(rs_con.getTimestamp("Time").getTime());

                        points += "<Placemark>\n" +
                                " <name>" + df.format(dt) + "</name>\n" +
                                " <description>Speed:" + rs_con.getInt("Speed") + "</description>\n" +
                                " <Point>\n" +
                                " <coordinates>" + coord + "</coordinates>" +
                                " </Point>\n" +
                                " </Placemark>";
                    }
                }
                line += "</coordinates></LineString></Placemark>";
            }

            result += line + "\n" + points + "</Document> </kml>";
            //stmt_con = con.prepareStatement("delete from Position where User=?");
            //stmt_con.setString(1, user);
            //stmt_con.execute();
            //con.commit();
        } catch (SQLException | NamingException e) {
            result = e.toString() + Arrays.toString(e.getStackTrace());
        }
        try {
            if (con!=null && !con.isClosed()) con.close();
        } catch (SQLException e) {
            //result+=e.toString()+Arrays.toString(e.getStackTrace());
        }
        return result;
    }
}
