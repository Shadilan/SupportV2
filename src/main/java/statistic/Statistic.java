package statistic;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import utility.DBUtils;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * @author Shadilan
 * @version 1.0
 * @since 22.06.2016
 */
public class Statistic {
    /**
     * Обрабатывает запрос и определяет необходимую операцию
     * @param request HttpServletRequest объект для обработки.
     * @return Возвращает результат обработки
     */
    public static String doRequest(HttpServletRequest request){
        try {
            String operation;
            try {
                operation = request.getParameter("Oper");
            } catch (Exception e) {
                return "{\"Result\":\"STE00001\",\"Message\":\"Operation not recognized\"}";
            }
            switch (operation) {
                case "doAction":
                    return doAction(readRequest(request));
                case "doPosition":
                    return doPosition(readRequest(request));
                case "doException":
                    return doException(readRequest(request));
                case "doKey":
                    String appName = request.getParameter("App");
                    return doApplication(appName);
                case "doRecount":
                    return RecountStat();
                default:
                    return "{\"Result\":\"STE00001\",\"Message\":\"Operation not recognized\"}";
            }

        } catch (Exception e){
            return "{\"Result\":\"STE00002\",\"Message\":\""+e.toString()+ Arrays.toString(e.getStackTrace())+"\"}";

        }
    }

    private static String doException(JsonObject object) {
        String result;
        String key="-";
        String user="-";
        String device="-";
        String group="-";
        String title="-";
        String version="-";
        String description="";
        if (object.has("Key")) key=object.get("Key").getAsString();
        if (object.has("User")) user=object.get("User").getAsString();
        if (object.has("Version")) version=object.get("Version").getAsString();
        if (object.has("Device")) device=object.get("Device").getAsString();
        if (object.has("Group")) group=object.get("Group").getAsString();
        if (object.has("Title")) title=object.get("Title").getAsString();
        if (object.has("Description")) description=object.get("Description").getAsString();
        Connection con = null;
        try {
            con= DBUtils.ConnectDB();
            PreparedStatement stmt;
            stmt=con.prepareStatement("INSERT into Exception(AppKey,Version,User,Device,GroupName,Title,Description) " +
                    "values(?,?,?,?,?,?,?)");
            stmt.setString(1,key);
            stmt.setString(2,version);
            stmt.setString(3,user);
            stmt.setString(4,device);
            stmt.setString(5,group);
            stmt.setString(6,title);
            stmt.setString(7, description);
            stmt.execute();
            stmt.close();
            con.commit();
            result="{\"Result\":\"OK\"}";
        } catch (SQLException | NamingException e) {
            result="{\"Result\":\"STE01000\",\"Message\":\""+e.toString()+ Arrays.toString(e.getStackTrace())+"\"}";
        }
        try {
            if (!con.isClosed()){
                con.close();
            }
        } catch (SQLException e) {
            result="{\"Result\":\"STE01000\",\"Message\":\""+e.toString()+ Arrays.toString(e.getStackTrace())+"\"}";
        }
        return result;
    }

    /**
     * Translate request to JSON
     * @param request  HttpServletRequest to translate
     * @return result of translation
     */
    public static JsonObject readRequest(HttpServletRequest request){
        StringBuilder stringBuffer=new StringBuilder();
        String req;
        try {
            BufferedReader reader=request.getReader();

            String line;
            while ((line=reader.readLine())!=null){
                stringBuffer.append(line);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        req=stringBuffer.toString();
        return (new JsonParser()).parse(req).getAsJsonObject();
    }
    public static String doApplication(String AppName){
        String result;
        Connection con;
        try{
            con=DBUtils.ConnectDB();
            PreparedStatement stmt;
            String key= UUID.randomUUID().toString();
            stmt=con.prepareStatement("INSERT INTO Applications(AppName,AppKey)values(?,?)");
            stmt.setString(1, AppName);
            stmt.setString(2, key);
            stmt.execute();
            con.commit();
            result= "{\"Result\":\"OK\",\"Key\":\""+key+"\"}";
        } catch (SQLException | NamingException e) {
            result="{\"Result\":\"STE01000\",\"Message\":\""+e.toString()+ Arrays.toString(e.getStackTrace())+"\"}";
        }

        return result;
    }
    /**
     * Add action to stat
     * @param object Info to add
     * @return Success or error
     */
    public static String doAction(JsonObject object){
        String result;
        String key="-";
        String user="-";
        String device="-";
        String group="-";
        String attribute="-";
        int hit=0;
        int time=0;
        if (object.has("Key")) key=object.get("Key").getAsString();
        if (object.has("User")) user=object.get("User").getAsString();
        if (object.has("Device")) device=object.get("Device").getAsString();
        if (object.has("Group")) group=object.get("Group").getAsString();
        if (object.has("Attribute")) attribute=object.get("Attribute").getAsString();
        if (object.has("Hit")) hit=object.get("Hit").getAsInt();
        if (object.has("Time")) time=object.get("Time").getAsInt();
        Connection con = null;
        try {
            con= DBUtils.ConnectDB();
            PreparedStatement stmt;
            stmt=con.prepareStatement("INSERT into TRANSACTION(AppKey,User,Device,GroupName,Attribute,Hit,Time) values(?,?,?,?,?,?,?)");
            stmt.setString(1,key);
            stmt.setString(2,user);
            stmt.setString(3,device);
            stmt.setString(4,group);
            stmt.setString(5,attribute);
            stmt.setInt(6, hit);
            stmt.setInt(7, time);
            stmt.execute();
            stmt.close();
            con.commit();
            result="{\"Result\":\"OK\"}";
        } catch (SQLException | NamingException e) {
            result="{\"Result\":\"STE01000\",\"Message\":\""+e.toString()+ Arrays.toString(e.getStackTrace())+"\"}";
        }
        try {
            if (!con.isClosed()){
                con.close();
            }
        } catch (SQLException e) {
            result="{\"Result\":\"STE01000\",\"Message\":\""+e.toString()+ Arrays.toString(e.getStackTrace())+"\"}";
        }
        return result;
    }

    public static String doPosition(JsonObject object){
        String result;
        String key="-";
        String user="-";
        String device="-";
        String group="-";
        int speed=0;
        int lat=0;
        int lng=0;
        if (object.has("User")) user=object.get("User").getAsString();
        if (object.has("Speed")) speed=object.get("Speed").getAsInt();
        if (object.has("Lat")) lat=object.get("Lat").getAsInt();
        if (object.has("Lng")) lng=object.get("Lng").getAsInt();

        Connection con = null;
        try {
            con= DBUtils.ConnectDB();
            PreparedStatement stmt;
            stmt=con.prepareStatement("INSERT into Position (User,Speed,Lat,Lng) values(?,?,?,?)");
            stmt.setString(1,user);
            stmt.setInt(2, speed);
            stmt.setInt(3, lat);
            stmt.setInt(4,lng);
            stmt.execute();
            stmt.close();
            con.commit();
            result="{\"Result\":\"OK\"}";
        } catch (SQLException | NamingException e) {
            result="{\"Result\":\"STE01000\",\"Message\":\""+e.toString()+ Arrays.toString(e.getStackTrace())+"\"}";
        }
        try {
            if (con!=null && !con.isClosed()){
                con.close();
            }
        } catch (SQLException e) {
            result="{\"Result\":\"STE01000\",\"Message\":\""+e.toString()+ Arrays.toString(e.getStackTrace())+"\"}";
        }
        return result;
    }

    public static String RecountStat(){
        //Connect DB
        Connection con=null;
        String result="";
        try {
            result+="Connecting DB\n";
            con=DBUtils.ConnectDB();
            //getSystemDate
            PreparedStatement stmt;
            ResultSet rs;

            java.util.Date date=new Date();

            result+="Date"+date.toString()+"\n";
            //Remove noKey
            result+="Create stmt\n";
            stmt=con.prepareStatement("delete from TRANSACTION where AppKey not in (select AppKey from Applications)");
            stmt.execute();
            //UpsertData into Users
            result+="Create stmt2\n";
            stmt=con.prepareStatement("INSERT IGNORE INTO Users( User, ApplicationId ) \n" +
                    "SELECT t.User, a.ID\n" +
                    "FROM TRANSACTION t, Applications a\n" +
                    "WHERE a.AppKey = t.AppKey\n" +
                    "GROUP BY t.User, a.ID");
            stmt.execute();
            result+="Create stmt3\n";
            //UpsertData into Device
            stmt=con.prepareStatement("INSERT IGNORE INTO Devices (Device,UserID)" +
                    " select t.Device,u.ID from TRANSACTION t,Applications a,Users u " +
                    "where a.AppKey=t.AppKey " +
                    "and u.ApplicationId=a.ID " +
                    "and u.User=t.User " +
                    "group by t.User,a.ID");
            stmt.execute();
            //UpsertData into Attributes
            result+="Create stmt4\n";
            stmt=con.prepareStatement("INSERT IGNORE INTO Attributes (GroupName,Attribute,ApplicationID)" +
            " select t.GroupName,t.Attribute,a.ID from TRANSACTION t,Applications a " +
                    "where a.AppKey=t.AppKey " +
                    "group by t.User,a.ID");
            stmt.execute();
            //UpsertData into Statistics
            result+="Create stmt5\n";
            stmt=con.prepareStatement("INSERT INTO Statistics(UserID,DeviceID,AttributeID,Hit,MinHit,MaxHit,Time,MinTime,MaxTime,Num,StatDay)\n" +
                    "SELECT u.ID UserID, d.ID DevID, at.ID AtrID, SUM( Hit ) , MIN( Hit ) , MAX( Hit ) , SUM( TIME ) , MIN( TIME ) , MAX( TIME ) , COUNT( 1 ),DATE_FORMAT(t.Creation, '%Y-%m-%d') \n" +
                    "FROM TRANSACTION t, Applications a, Users u, Devices d, Attributes at\n" +
                    "WHERE t.AppKey = a.AppKey\n" +
                    "AND u.ApplicationID = a.ID\n" +
                    "AND at.ApplicationID = a.ID\n" +
                    "AND d.UserID = u.ID\n" +
                    "AND u.User = t.User\n" +
                    "AND d.Device = t.Device\n" +
                    "AND at.GroupName = t.GroupName\n" +
                    "AND at.Attribute = t.Attribute\n" +
                    "AND UNIX_TIMESTAMP( Creation ) < ? \n" +
                    "GROUP BY at.ID, d.ID, u.ID \n" +
                    "ON DUPLICATE KEY UPDATE\n" +
                    "Hit=values(Hit)+Hit, Time=values(Time)+Time, Num=values(Num)+Num, \n" +
                    "MinHit=case when MinHit<values(MinHit) then MinHit else values(MinHit) end, \n" +
                    "MinTime=case when MinTime<values(MinTime) then MinTime else values(MinTime) end, \n" +
                    "MaxHit=case when MaxHit<values(MaxHit) then MaxHit else values(MaxHit) end, \n" +
                    "MaxTime=case when MaxTime<values(MaxTime) then MaxTime else values(MaxTime) end ");
            stmt.setLong(1, date.getTime());
            stmt.execute();
            result+="Create stmt6\n";
            result+="Updated"+stmt.getUpdateCount()+"\n";
            //Delete data
            stmt=con.prepareStatement("delete from TRANSACTION where UNIX_TIMESTAMP( Creation ) < ?");
            stmt.setLong(1, date.getTime());
            stmt.execute();
            result+="Create stmt7\n";
            result+="Deleted"+stmt.getUpdateCount()+"\n";
            String sql_text="INSERT INTO Statistics_All( UserID, DeviceID, AttributeID, MinHit, MaxHit, Hit, MinTime, MaxTime, TIME, Num ) \n" +
                    "SELECT UserID, DeviceID, AttributeID, MIN( MinHit ) , MAX( MaxHit ) , SUM( Hit ) , MIN( MinTime ) , MAX( MaxTime ) , SUM( TIME ) , SUM( Num ) \n" +
                    "FROM Statistics\n" +
                    "WHERE StatDay = NULL \n" +
                    "OR StatDay < NOW( ) - INTERVAL 10 DAY \n" +
                    "GROUP BY UserID, DeviceId, AttributeID ON DUPLICATE \n" +
                    "KEY UPDATE Hit = VALUES (Hit) + Hit, TIME = \n" +
                    "VALUES (TIME) + TIME, Num = \n" +
                    "VALUES (Num) + Num, MinHit = CASE WHEN MinHit < VALUES (MinHit)\n" +
                    "THEN MinHit ELSE VALUES (MinHit)\n" +
                    "END , MinTime = \n" +
                    "CASE WHEN MinTime < \n" +
                    "VALUES (MinTime)\n" +
                    "THEN MinTime\n" +
                    "ELSE VALUES (MinTime) END , MaxHit = CASE WHEN MaxHit < VALUES (MaxHit)\n" +
                    "THEN MaxHit\n" +
                    "ELSE VALUES (MaxHit) END , MaxTime = CASE WHEN MaxTime < VALUES (MaxTime)\n" +
                    "THEN MaxTime\n" +
                    "ELSE VALUES (MaxTime) END";
            stmt=con.prepareStatement(sql_text);
            stmt.execute();
            result+="Updated"+stmt.getUpdateCount()+"\n";
            //Delete data
            result+="Create stmt8\n";
            stmt=con.prepareStatement("delete from Statistics where StatDay=NULL or StatDay < NOW( ) - INTERVAL 10 DAY");
            stmt.execute();
            result+="Deleted"+stmt.getUpdateCount()+"\n";
            stmt=con.prepareStatement("delete from Position where Time < NOW( ) - INTERVAL 1 DAY");
            stmt.execute();
            result+="Deleted Pos:"+stmt.getUpdateCount()+"\n";

            //commit
            con.commit();

        } catch (SQLException | NamingException e) {
            result+=e.toString()+Arrays.toString(e.getStackTrace());
        }
        try {
            if (con!=null && !con.isClosed()) con.close();
        } catch (SQLException e) {
            //result+=e.toString()+Arrays.toString(e.getStackTrace());
        }
        return result;
    }
}
