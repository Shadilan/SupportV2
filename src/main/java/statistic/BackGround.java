package statistic;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.naming.NamingException;
import java.sql.SQLException;

@Singleton
@TransactionManagement(TransactionManagementType.BEAN)
public class BackGround {
    @Schedule(hour="*", minute="5", second="0", persistent=false)
    public void Hourly() throws SQLException, NamingException {
        Statistic.RecountStat();
    }

}
